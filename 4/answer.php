<?php
require_once('secondTaskFunctions.php');
$globalSum = 0;
function readTillNumber($line){
    $value = '';
    $weight = '';
    $i = 0;
    while (!is_numeric($line[$i])){
        $value = $value.$line[$i];
        $i++;
    }
    while($i < strlen($line) && is_numeric($line[$i])){
        $weight = $weight.$line[$i];
        $i++;
    }
    $GLOBALS["globalSum"]+= (int)$weight;
    return [
        "text" => trim($value),
        "weight" => $weight,
        "probability" => 0

    ];
}
function dividedByLines($inputLine){
    $outputArray = [];
    $iterator = 0;
    $start = 0;
    $finish = mb_strpos($inputLine, PHP_EOL, $start);
    while ($finish) {
        $outputArray[$iterator] = readTillNumber(mb_substr($inputLine, $start, (intval($finish) - intval($start))));
        $iterator++;
        $start = ++$finish;
        $finish = mb_strpos($inputLine, PHP_EOL, $start);
    }
    if (--$start != mb_strlen($inputLine)) {
        $outputArray[$iterator] = readTillNumber(mb_substr($inputLine, $start, mb_strlen($inputLine)));
    }
    return $outputArray;
}

$input = dividedByLines(($_REQUEST["line"]));
$output = [
    "sum" => $GLOBALS["globalSum"],
    "data" => []
];
echo("First task <br/>\n");
foreach ($input as &$element) {
    $element["probability"] = (int)$element["weight"] / $GLOBALS["globalSum"];
    array_push($output["data"],$element);
    //print_r(json_encode($element, JSON_UNESCAPED_UNICODE));
    //echo("<br/>\n");
}
print_r(json_encode($output, JSON_UNESCAPED_UNICODE));
echo("<br/>\nSecond task <br/>\n");
uasort($output["data"], function (array $f1,array $f2)
{
    return $f2["probability"] <=> $f1["probability"];
});
foreach ($output["data"] as &$element){
    $element["count"] = 0;
}
foreach (lineGenerator($output["data"]) as $key){
    $output["data"][$key]["count"]++;
}
foreach ($output["data"] as &$element){
    $element["calculated_probability"] = $element["count"] / 10000;
}

ksort($output["data"]);
print_r(json_encode($output,JSON_UNESCAPED_UNICODE));

