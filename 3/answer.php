<?php
function dividedArray($inputLine, $diveSymbol){
    $outputArray = [];
    $iterator = 0;
    $start = 0;
    $finish = mb_strpos($inputLine, $diveSymbol, $start);
    while ($finish) {
        $outputArray[$iterator] = mb_substr($inputLine, $start, (intval($finish) - intval($start)));
        $iterator++;
        $start = ++$finish;
        $finish = mb_strpos($inputLine, $diveSymbol, $start);
    }
    if (--$start != mb_strlen($inputLine)) {
        $outputArray[$iterator] = mb_substr($inputLine, $start, mb_strlen($inputLine));
    }
    return $outputArray;
}
function randomChange(array $inputLine){
    $firstIndex = rand(0, count($inputLine) - 1);
    $secondIndex = rand(0, count($inputLine) - 1);
    while ($firstIndex == $secondIndex){
        $secondIndex = rand(0, count($inputLine) - 1);
    }
    $temp = $inputLine[$firstIndex];
    $inputLine[$firstIndex] = $inputLine[$secondIndex];
    $inputLine[$secondIndex] = $temp;
    return $inputLine;
}
$output = dividedArray($_REQUEST["line"], PHP_EOL);

for($i = 0; $i < count($output); $i++){
    $output[$i] = dividedArray($output[$i], ' ');
}
$finishCount = count($output);
for($j = 0; $j < $finishCount; $j++){
    array_push($output,randomChange($output[$j]));
}
uasort($output, function (array $f1, array $f2){
    return $f1[1] <=>$f2[1];
});

for($i = 0; $i < count($output); $i++){
    $output[$i] = implode(($output[$i]), ' ');
}
echo implode($output, '<br/>');


