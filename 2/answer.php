<?php
    $changeCount = 0;
    function changeGenerator($line){
        for($i = 0; $i < strlen($line); $i++){
            switch ($line[$i]){
                case 'h':
                    yield '4';
                    $GLOBALS['changeCount']++;
                    break;
                case 'l':
                    yield '1';
                    $GLOBALS['changeCount']++;
                    break;
                case 'e':
                    yield '3';
                    $GLOBALS['changeCount']++;
                    break;
                case 'o':
                    yield '0';
                    $GLOBALS['changeCount']++;
                    break;
                default:
                    yield $line[$i];
                    break;
            }
        }
    }
    function generatorUser($line){
        $generated = changeGenerator($line) ;
        $output = "";
        foreach ($generated as $i){
            $output = $output.$i;
        }
        return($output);
//        echo "</br> number of changes = ".$GLOBALS['changeCount'];
    }
    echo generatorUser($_REQUEST["line"]);
    echo "</br> number of changes = ".$GLOBALS['changeCount'];
