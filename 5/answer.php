<?php
$psw_is_correct = true;
if(mb_strlen($_REQUEST["password"]) < 10){
    $psw_is_correct = false;
    echo ("Пароль должен содержать не мнее 10-ти символов"."<br/>\n");
}
if(!preg_match('/(.*[a-z].*){2,}/', $_REQUEST["password"]) ||
        !preg_match('/(.*[A-Z].*){2,}/', $_REQUEST["password"]) ||
            !preg_match('/(.*[0-9].*){2,}/', $_REQUEST["password"]) ||
                !preg_match('/(.*(%|\$|#|_|\*).*){2,}/', $_REQUEST["password"])){
    $psw_is_correct = false;
    echo("Пароль не содержит необходимых символов"."<br/>\n");
}
if(preg_match('/[a-z]{3,}/', $_REQUEST["password"]) ||
        preg_match('/[A-Z]{3,}/', $_REQUEST["password"]) ||
            preg_match('/[0-9]{3,}/', $_REQUEST["password"]) ||
                preg_match('/(%|\$|#|_|\*){3,}/', $_REQUEST["password"])){
    $psw_is_correct = false;
    echo ("Пароль содержит слишком много специальных символов подряд"."<br/>\n");
}
if($psw_is_correct){
    echo ("Пароль успешно принят");
}

