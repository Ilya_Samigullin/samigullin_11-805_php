<?php
$config = parse_ini_file("index.ini", true, INI_SCANNER_NORMAL);
$input = file("input.txt");
foreach ($input as $line){
    if(start_comparation($line, $config["first_rule"]["symbol"])){
        echo (first_rule($line,$config["first_rule"]["upper"])."<br/>\n");
    }
    else if(start_comparation($line, $config["second_rule"]["symbol"])){
        echo (second_rule($line,$config["second_rule"]["direction"])."<br/>\n");
    }
    else if(start_comparation($line, $config["third_rule"]["symbol"])){
        echo (third_rule($line,$config["third_rule"]["delete"])."<br/>\n");
    }
    else{
        echo ($line."<br/>\n");
    }
}
function start_comparation(&$input_line, $input_symbol){
    $lineStart = '';
    for($i = 0; $i < mb_strlen($input_symbol); $i++){
        $lineStart = $lineStart.$input_line[$i];
    }
    if($lineStart == $input_symbol) {
        $input_line = substr($input_line, $i);
        return true;
    }
    return false;
}

function first_rule($input_line,$upper){
    if($upper != 'true' && $upper != 'false'){
        return("invalid configs");
    }
    /*$output = mb_str_split($input_line);
    foreach ($output as $symbol){
        if(mb_strtoupper($symbol) == mb_strtolower($symbol) && $symbol != PHP_EOL){
            return("invalid input");
        }
    }*/
    if($upper){
        return(mb_strtoupper($input_line));
    }
    else{
        return(mb_strtolower($input_line));
    }
}

//function mb_str_split($string,$string_length=1) { //взято с гитхаба
//    if(mb_strlen($string) > $string_length || !$string_length) {
//        do {
//            $c = mb_strlen($string);
//            $parts[] = mb_substr($string,0,$string_length);
//            $string = mb_substr($string,$string_length);
//        }while(!empty($string));
//    } else {
//        $parts = array($string);
//    }
//    return $parts;
//}

function second_rule($input_line,$direction){
    if($direction != '+' && $direction != '-'){
        return("invalid configs");
    }
    $minusdirection = array();
    $minusdirection['0'] = '9';
    $minusdirection['1'] = '0';
    $minusdirection['2'] = '1';
    $minusdirection['3'] = '2';
    $minusdirection['4'] = '3';
    $minusdirection['5'] = '4';
    $minusdirection['6'] = '5';
    $minusdirection['7'] = '6';
    $minusdirection['8'] = '7';
    $minusdirection['9'] = '8';
    $plusdirection = array();
    $plusdirection['0'] = '1';
    $plusdirection['1'] = '2';
    $plusdirection['2'] = '3';
    $plusdirection['3'] = '4';
    $plusdirection['4'] = '5';
    $plusdirection['5'] = '6';
    $plusdirection['6'] = '7';
    $plusdirection['7'] = '8';
    $plusdirection['8'] = '9';
    $plusdirection['9'] = '0';
//    if (preg_match('/\-[0-9]+/', $input_line)) {
//        if (preg_match('/[^\-][0-9]+/', $input_line)) {
//              return("invalid input");
//        }
//        if($direction == '+'){
//            $input_line = strtr($input_line, $minusdirection);
//        }
//        else{
//            $input_line = strtr($input_line, $plusdirection);
//        }
//    }
//    else {
//        if ($direction == '-') {
//            $input_line = strtr($input_line, $minusdirection);
//        } else {
//            $input_line = strtr($input_line, $plusdirection);
//        }
//    }
    if ($direction == '-') {
        $input_line = strtr($input_line, $minusdirection);
    } else {
        $input_line = strtr($input_line, $plusdirection);
    }
    return $input_line;
}

function third_rule($input_line,$to_delete){
    $input_line = str_replace($to_delete,'',$input_line);
    return $input_line;
}


